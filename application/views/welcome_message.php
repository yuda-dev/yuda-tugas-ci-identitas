<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Biodata</title>
    <style>
      body {
        background-color: #e8e8e8;
      }
      .kartu {
        width: 800px;
        margin: 0 auto;
        margin-top: 70px;
            box-shadow: 0 0.25rem 0.75rem rgba(0,0,0,.03);
    transition: all .3s;
      } 
      .foto {
        padding: 20px;
      }
      tbody {
    font-size: 20px;
    font-weight: 300;
}
.biodata {
    margin-top: 30px;
}
    </style>
  </head>
  <body>

<div class="container">
  <div class="card kartu">
    <div class="row">
      <div class="col-md-4">
      <div class="foto">
        <img src="https://spektaweb.com/uploads/kapten2.jpg" alt="" width="200" height="auto">
      </div>
      </div>
      <div class="col-md-8 kertas-biodata">
        <div class="biodata">
        <table width="100%" border="0">
      <tbody><tr>
        <td valign="top">
        <table border="0" width="100%" style="padding-left: 2px; padding-right: 13px;">
          <tbody>
            <tr>
              <td width="25%" valign="top" class="textt">Nama</td>
                <td width="2%">:</td>
                <td style="color: rgb(118, 157, 29); font-weight:bold">Yuda Muhtar</td>
            </tr>
          <tr>
              <td class="textt">Jenis Kelamin</td>
                <td>:</td>
                <td>Laki-Laki</td>
            </tr>
          <tr>
              <td class="textt">Tempat Lahir</td>
                <td>:</td>
                <td>Bandung,</td>
            </tr>
          <tr>
              <td class="textt">Tanggal Lahir</td>
                <td>:</td>
                <td>27/04/1996</td>
            </tr>
          <tr>
              <td class="textt">Kelas</td>
                <td>:</td>
                <td>TIF-PK17</td>
            </tr>
          <tr>
              <td valign="top" class="textt">Jurusan</td>
                <td valign="top">:</td>
                <td>Teknik Informatika</td>
            </tr>
            <tr>
              <td valign="top" class="textt">Blog</td>
                <td valign="top">:</td>
                <td>www.spektaweb.com</td>
            </tr>
        </tbody>
      </table>
        </td>
    </tr>
    </tbody>
  </table>
  </div>
      </div>
    </div>
  </div>
</div>
    
  </body>
</html>